import collections
import random

chiffres, lettres = '0123456789', 'abcdefghijklmnopqrstuvwxyz'

regles = (line.strip() for line in open('pop.txt') if line.strip() and not line.startswith("//"))


def valgen(dep, diff = 0, sent=-1):
    while dep != sent:
        dep = (yield dep) or dep + diff


jours_arrivees = valgen(dep=0)

arrivees = collections.defaultdict(list)
for ligne in regles:
    if ligne.startswith('='):
        jours_arrivees.send(int(''.join(_ for _ in ligne if _ in chiffres)))
    else:
        arrivees[next(jours_arrivees)].append(ligne)

PLANTE, MEROU, THON, PCLOWN, SOLE, BAR, CARPE = valgen(dep=0, diff=1, sent=7)
carnivores = {MEROU, THON, PCLOWN}
herbivores = {SOLE, BAR, CARPE}
tous = carnivores | herbivores | {PLANTE}

aquarium = []
poissons = (e for e in aquarium if e.espece in herbivores | carnivores)
algues = (e for e in aquarium if e.espece == PLANTE)


class Entite:
    age = None
    pv = None
    espece = None
    pv_gagnes_par_tour = None
    regne = None
    cible = None

    def __next__(self):
        try: # contrairement a next(), il faut utiliser un bloc try/catch avec send(), et donc un bloc fonction et class
            self.age.send(next(self.age) + 1)
        except StopIteration:
            pass
        try:
            self.pv.send(next(self.pv) + self.pv_gagnes_par_tour)
        except StopIteration:
            pass
        if self.espece != PLANTE and next(self.pv) <= 5:
            self.manger()

    def manger(self):
        cible = None

        try:
            cible = {
                "carn": lambda _: random.choice(poissons),
                "herb": lambda _: random.choice(algues)
            }.get(self.regne)
            cible.pv.send(max(0, next(cible.pv) - cible.pv_perdu_si_mange))
        except StopIteration:
            aquarium.remove(cible)

    def se_reproduire(self):
        test = {
            PLANTE: lambda self:next(self.pv) >= 10
        }.get(self.espece, lambda self: random.choice(poissons).sexuellement_compatible(self))


def gen_entite(prenom, espece, age):
    ent = Entite()
    ent.prenom = prenom
    ent.espece = espece
    ent.regne = lambda self: "carn" if self.espece in carnivores else "herb" if self.espece in herbivores else "plante"
    ent.pv = valgen(dep=10, diff=0, sent=0)
    ent.age = valgen(dep=age, diff=0, sent=20)
    ent.en_vie = lambda self: next(self.pv, False) is not False and next(self.age, False) is not False
    ent.pv_gagnes_par_tour = +1 if espece == PLANTE else -1
    ent.pv_perdu_si_mange = -2 if espece == PLANTE else -4
    ent.a_faim = lambda self: next(self.pv) <= 5
    ent.sexuellement_compatible = lambda self, other: self.espece == other.espece and self.sexe() != other.sexe()

    return ent
